//pins ultrasonic
int triggerPort = 12;
int echoPort = 11;

//color pins
const int redPin = 4;
const int greenPin = 3;
const int bluePin = 1;

// Init our Vars
int colorRed;
int colorGreen;
int colorBlue;

void setup() {

  pinMode(triggerPort, OUTPUT);
  pinMode(echoPort, INPUT);
  
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
  
}

void loop() {
  
  digitalWrite(triggerPort, LOW);
  digitalWrite(triggerPort, HIGH);
  delayMicroseconds(10);
  digitalWrite( triggerPort, LOW);
  
  long time = pulseIn(echoPort, HIGH);
  long range = 0.034 * time / 2;

  colorRed = (range*20) % 255;
  colorBlue = (range*30) % 255;
  colorGreen = (range*50) % 255;
  
  analogWrite(redPin, colorRed);
  analogWrite(bluePin, colorBlue);
  analogWrite(greenPin, colorGreen);
  
  delay( 400 );
}
