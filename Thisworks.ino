//This works
int redPin = 4;//ont is an integer
int greenPin = 3;
int bluePin = 2;

int redPin2 = 7;
int greenPin2 = 6;
int bluePin2 = 8;

int redPin3 = 12;
int greenPin3 = 11;
int bluePin3 = 13;

void setup() {
  Serial.begin(9600);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
  
  pinMode(redPin2, OUTPUT);
  pinMode(greenPin2, OUTPUT);
  pinMode(bluePin2, OUTPUT);

  pinMode(redPin3, OUTPUT);
  pinMode(greenPin3, OUTPUT);
  pinMode(bluePin3, OUTPUT);
}

//initilize n and cout as globle
int n = 0;
int count;

void loop() {
  setColor(25,51 ,0);
  delay(50);
  setColor(51,102,0);
  delay(50);
  setColor(76, 153, 0); 
  delay(50);
  setColor(102,204 ,0);
  delay(50);
  setColor(128,255 ,0);
  delay(50);
  setColor(153,255 ,51);
  delay(50);
  setColor(255,255 ,153);
  delay(50);
  setColor(255,255 ,102);
  delay(50);
  setColor(255,255 ,51);
  delay(50);
   setColor(255,255 ,0);
  delay(50);
   setColor(255,128 ,0);
  delay(50);
   setColor(255,153 ,51);
  delay(50);
   setColor(255,178 ,102);
  delay(50);
   setColor(255,204 ,153);
  delay(50);
    setColor(255,153 ,153);
  delay(50);
   setColor(255,102 ,102);
  delay(50);
   setColor(255,51 ,51);
  delay(50);
   setColor(150,0 ,0);
  delay(50);
   n = n + 1;
   if(n > 255) n = 0;
   delay(100);
   Serial.print(n);
   Serial.print(" ");
   count++;
  if (count % 20 == 0)Serial.println();
  
}

void setColor(int redValue, int greenValue, int blueValue) {
  analogWrite(redPin, redValue);
  analogWrite(greenPin, greenValue);
  analogWrite(bluePin, blueValue);
  
  analogWrite(redPin2, redValue);
  analogWrite(greenPin2, greenValue);
  analogWrite(bluePin2, blueValue);

  analogWrite(redPin3, redValue);
  analogWrite(greenPin3, greenValue);
  analogWrite(bluePin3, blueValue);

}
